import alphabet.E;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MoreTests {
    @Test
    public void testE() {
        assertEquals("I'm E", new E().toString());
    }

    @Test
    public void testNumberOfAlphabet() {
        assertEquals(5, new E().getLetterOfAlphabet());
    }

    @Test
    public void testNameAgainstRegex() {
        Pattern pattern = Pattern.compile(".{4}E");
        Matcher matcher = pattern.matcher(new E().toString());

        assertTrue(matcher.find());
    }
}
