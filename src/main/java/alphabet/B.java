package alphabet;

public class B extends A {
    private final String name = "I'm b";

    @Override
    public String toString() {
        return this.name;
    }
}
