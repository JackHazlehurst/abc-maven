package alphabet;

import com.google.common.collect.ImmutableMap;

public class D extends B {
    private final String name = "I'm d";

    public String getShopping() {
        ImmutableMap<String, Integer> items = ImmutableMap.of(
                "apples", 2,
                "oranges", 7,
                "grapes", 0);

        return items.toString();
    }
}
